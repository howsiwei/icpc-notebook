# Build instructions

To build pdf, use the following command:
```sh
latexmk -lualatex notebook.ltx
```

if encounter
```
! LaTeX Error: File `<package-name>.sty' not found.
```

install the package by
```
tlmgr install <package-name>
```
