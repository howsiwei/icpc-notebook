// HLD (with subtree stuff) - 

#include <bits/stdc++.h>
using namespace std;

#define ll long long
const int N = 1e5 + 5;

int n;
vector<int> adj[N];
set<int> S[N];
int sz[N], p[N], h[N], loc[N];
int chainNo[N], head[N], start[N], finish[N], chainCnt = 0, cntr = 0;
ll lazy[4*N];
pair<ll, int> seg[4*N];

void dfs(int u, int prev){
    sz[u] = 1;
    p[u] = prev;
    h[u] = h[prev] + 1;
    for(auto v : adj[u]){
        if(v == prev)   continue;
        dfs(v, u);
        sz[u] += sz[v];
    }
}

void hld_construct(int u, int prev){

    start[u] = ++cntr;

    if(chainNo[u] == -1){
        chainNo[u] = ++chainCnt;
        head[chainCnt] = u;
    }
    
    int bestChild = 0;
    
    for(auto v : adj[u]){
        if(v == prev)   continue;
        if(sz[v] > sz[bestChild])   bestChild = v;
    }

    if(bestChild != 0){
        chainNo[bestChild] = chainNo[u];
        hld_construct(bestChild, u);
    }

    for(auto v : adj[u]){
        if(v == prev or v == bestChild)   continue;
        hld_construct(v, u);
    }

    finish[u] = cntr;
}

pair<ll, int> combine(pair<ll, int> a, pair<ll, int> b){
    if(a.second == -1)  return b;
    if(b.second == -1)  return a;
    if(make_pair(a.first + a.second, loc[a.second]) < make_pair(b.first + b.second, loc[b.second]))   return a;
    else return b;
}

void push_lazy(int nd, int l, int r){
    if(!lazy[nd])   return ;
    seg[nd].first += lazy[nd];
    if(l != r){
        lazy[nd*2] += lazy[nd];
        lazy[nd*2 + 1] += lazy[nd];
    }
    lazy[nd] = 0;
}

void point_update(int nd, int l, int r, int x, int indice){
    push_lazy(nd, l, r);
    if(l > x or r < x)  return ;
    if(l == r){
        if(S[indice].empty())    seg[nd] = {0, -1};
        else    seg[nd] = {seg[nd].first, *S[indice].begin()};
    }
    else{
        int mid = (l + r)/2;
        point_update(nd*2 + 1, mid + 1, r, x, indice);
        point_update(nd*2, l, mid, x, indice);
        seg[nd] = combine(seg[nd*2], seg[nd*2 + 1]);
    }
}

pair<ll, int> query_segtree(int nd, int l, int r, int x, int y){
    push_lazy(nd, l, r);
    if(l > y or r < x)  return {0, -1};
    else if(l >= x and r <= y)  return seg[nd];
    else{
        int mid = (l + r)/2;
        return combine(query_segtree(nd*2, l, mid, x, y), query_segtree(nd*2 + 1, mid + 1, r, x, y));
    }
}

int lca(int u, int v){
    while(chainNo[u] != chainNo[v]){
        if(h[head[chainNo[v]]] > h[head[chainNo[u]]]) swap(u, v);
        u = p[head[chainNo[u]]];
    }
    if(h[u] > h[v]) return v;
    else return u;
}

pair<ll, int> query_path(int u, int par){
    pair<ll, int> ans = {0, -1};
    while(chainNo[u] != chainNo[par]){
        ans = combine(ans, query_segtree(1, 1, n, start[head[chainNo[u]]], start[u]));
        u = p[head[chainNo[u]]];
    }
    ans = combine(ans, query_segtree(1, 1, n, start[par], start[u]));
    return ans;
}   

void range_update(int nd, int l, int r, int x, int y, int val){
    push_lazy(nd, l, r);
    if(l > y or r < x)  return ;
    else if(l >= x and r <= y){
        lazy[nd] = val;
        push_lazy(nd, l, r);
    }
    else{
        int mid = (l + r)/2;
        range_update(nd*2, l, mid, x, y, val);
        range_update(nd*2 + 1, mid + 1, r, x, y, val);
        seg[nd] = combine(seg[nd*2], seg[nd*2 + 1]);
    }
}

int query(int u, int v){
    int LCA = lca(u, v);
    pair<ll, int> best = combine(query_path(u, LCA), query_path(v, LCA));
    if(best.second == -1)   return best.second;
    S[loc[best.second]].erase(best.second);
    point_update(1, 1, n, start[loc[best.second]], loc[best.second]);
    return best.second;
}

int main(){

    memset(chainNo, -1, sizeof(chainNo));

    int m, q, u, v, tmp, type, k;
    
    scanf("%d %d %d", &n, &m, &q);

    for(int i = 1; i < n; i++){
        scanf("%d %d", &u, &v);
        adj[u].push_back(v);
        adj[v].push_back(u);
    }
    for(int i = 1; i <= m; i++){
        scanf("%d", &tmp);
        loc[i] = tmp;
        S[tmp].insert(i);
    }

    dfs(1, 0);
    hld_construct(1, 0);
    for(int i = 1; i <= n; i++) point_update(1, 1, n, start[i], i);

    while(q--){
        scanf("%d", &type);
        if(type == 1){
            scanf("%d %d %d", &u, &v, &k);
            vector<int> V;
            while(k--){
                int ans = query(u, v);
                if(ans == -1)    break;
                V.push_back(ans);
            }
            cout<<V.size()<<" ";
            for(auto v : V) cout<<v<<" ";
            cout<<'\n';
        }
        else if(type == 2){
            scanf("%d %d", &u, &k);
            range_update(1, 1, n, start[u], finish[u], k);
        }
    }

    return 0;
}
