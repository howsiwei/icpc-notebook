#include <bits/stdc++.h>
using namespace std;

#define ll long long
const int N = 5005;

int A[N];
pair<int, int> B[N];

ll dp[2][N], cost[N];

void DnC(int hole_no, int l, int r, int lo, int hi, int limit){
	if(l > r)   return ;

	int mid = (l + r)/2, optimal_mid = mid - 1;

	dp[hole_no][mid] = dp[!hole_no][mid];
	for(int k = max(mid - 1, lo); k <= hi; k++){
		if(k - mid + 1 > limit) break;
		ll tmp = dp[!hole_no][k + 1] + cost[k] - cost[mid - 1];
		if(dp[hole_no][mid] > tmp){
			dp[hole_no][mid] = tmp;
			optimal_mid = k;
		}
	}
	DnC(hole_no, l, mid - 1, lo, optimal_mid, limit);
	DnC(hole_no, mid + 1, r, optimal_mid, hi, limit);
}

int main(){

	int n, m;
	cin>>n>>m;

	for(int i = 1; i <= n; i++) cin>>A[i];
	for(int i = 1; i <= m; i++) cin>>B[i].first>>B[i].second;

	sort(A + 1, A + n + 1);
	sort(B + 1, B + m + 1);

	dp[1][n + 1] = 0;
	for(int i = 1; i <= n; i++) dp[1][i] = 1e14;

	int bit = 0;

	for(int i = m; i >= 1; i--){

		for(int j = 1; j <= n; j++) cost[j] = cost[j - 1] + abs(B[i].first - A[j]);

		DnC(bit, 1, n, 1, n, B[i].second);
		bit = !bit;
	}
	ll ans = dp[!bit][1];
	if(ans >= 1e14) ans = -1;
	cout<<ans<<endl;
}
