// Palindromic Tree

#include "bits/stdc++.h"
using namespace std;
const int N = 1e5 + 5;
int n;
char str[N];
int q;
int k;
int ans[N];
struct node{
	int next[26];
	int len;
	int suflink;
	node(){
		memset(next , 0 , sizeof(next));
		suflink = 0;
		len = 0;
	}
};
node tree[N];
int num;
int suff;
vector < int > v[N];
int cnt[N];
void init(){
	num = 2;
	suff = 2;
	tree[1].len = -1;
	tree[1].suflink = 1;
	tree[2].len = 0;
	tree[2].suflink = 1;
	v[1].emplace_back(2);
}
bool add(int idx){
	int cur = suff;
	int let = str[idx] - 'a';
	while(1){
		int leng = tree[cur].len;
		if(idx - leng - 1 >= 1 && str[idx] == str[idx - leng - 1]){
			break;
		}
		cur = tree[cur].suflink;
	}
	if(tree[cur].next[let]){
		suff = tree[cur].next[let];
		return 0;
	}
	++num;
	suff = num;
	tree[num].len = tree[cur].len + 2;
	tree[cur].next[let] = num;
	if(tree[num].len == 1){
		tree[num].suflink = 2;
		v[2].emplace_back(num);
		return 1;
	}
	while(1){
		cur = tree[cur].suflink;
		int leng = tree[cur].len;
		if(idx - leng - 1 >= 1 && str[idx] == str[idx - leng - 1]){
			tree[num].suflink = tree[cur].next[let];
			break;
		}
	}
	v[tree[num].suflink].emplace_back(num);
	return 1;
}
void dfs(int node){
	for(int next : v[node]){
		dfs(next);
		cnt[node] += cnt[next];
	}
}
int main(){
	scanf("%s" , str + 1);
	n = strlen(str + 1);
	init();
	for(int i = 1 ; i <= n ; ++i){
		add(i);
		++cnt[suff];
	}
	dfs(1);
	for(int i = 3 ; i <= num ; ++i){
		ans[cnt[i]] = max(ans[cnt[i]] , tree[i].len);
	}
	for(int i = n - 1 ; i >= 1 ; --i){
		ans[i] = max(ans[i] , ans[i + 1]);
	}
	scanf("%d" , &q);
	while(q--){
		scanf("%d" , &k);
		printf("%d\n" , ans[k]);
	}
} 
