class SD {
public:
	int n;
	const char* s;
	vector<int> sa;
	vector<int> rank;
	vector<int> lcp;

	SD(int _n, const char* _s) : n(_n), s(_s) {
		buildsa();
		buildlcp();
	}

	void buildsa() {
		sa.resize(n);
		iota(begin(sa), end(sa), 0);
		sort(begin(sa), end(sa), [&](int x, int y) {
			return s[x] < s[y];
		});
		rank.resize(n);
		int cnt = 0;
		vector<int> pos(n);
		for (int i = 0; i < n; i++) {
			pos[cnt] = i;
			cnt += i == 0 || s[sa[i-1]] != s[sa[i]];
			rank[sa[i]] = cnt-1;
		}
		vector<int> oldsa(n);
		vector<int> oldrank(n);
		for (int len = 1; cnt < n; len *= 2) {
			swap(sa, oldsa);
			for (int i = n-len; i < n; i++) {
				sa[pos[rank[i]]++] = i;
			}
			for (auto j: oldsa) if (j >= len) {
				int i = j-len;
				sa[pos[rank[i]]++] = i;
			}
			swap(rank, oldrank);
			cnt = 0;
			for (int i = 0; i < n; i++) {
				pos[cnt] = i;
				cnt += i == 0
					|| oldrank[sa[i-1]] < oldrank[sa[i]]
					|| oldrank[sa[i-1]+len] < oldrank[sa[i]+len];
				rank[sa[i]] = cnt-1;
			}
		}
	}

	void buildlcp() {
		lcp.resize(n+1);
		lcp[0] = 0;
		lcp[n] = 0;
		for (int i = 0, len = 0; i < n-1; i++) {
			int h = sa[rank[i]-1];
			while (s[h+len] == s[i+len]) {
				len++;
			}
			lcp[rank[i]] = len;
			len = max(len-1, 0);
		}
	}

};

void solve(string s) {
	int n = SZ(s)+1;
	SD sd(n, s.c_str());
	vector<int> ans(n, 0);
	stack<pair<int,int>> t;
	t.emplace(0, 0);
	for (int i = 1; i <= n; i++) {
		int lb = i-1;
		int len = n-1-sd.sa[i-1];
		while (sd.lcp[i] < t.top().second) {
			ans[len] = max(ans[len], i-lb);
			tie(lb, len) = t.top();
			t.pop();
		}
		ans[len] = max(ans[len], i-lb);
		if (sd.lcp[i] > t.top().second) {
			t.emplace(lb, sd.lcp[i]);
		}
	}
}
