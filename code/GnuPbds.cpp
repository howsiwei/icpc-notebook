// GNU policy-based data structures
// This is an order statistics tree
// For usage as a map with values, change null_type to the value type
// Keys are unique - duplicate keys are not supported
// emplace() does not work, use insert() instead
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds; // older version in namespace pb_ds
typedef tree<unsigned,null_type,std::less<unsigned>,rb_tree_tag,tree_order_statistics_node_update> stat_tree;
// older version uses null_mapped_type

// count elements greater than or equal to key:
template <typename Tree, typename Key>
size_t count_greater_equal(Tree& tr,const Key& key){
    return tr.size()-tr.order_of_key(key);
}
// count elements less than key:
template <typename Tree, typename Key>
size_t count_less(Tree& tr,const Key& key){
    return tr.order_of_key(key);
}
// get iterator to ith smallest element (zero-based):
// returns tr.end() if there are at most i elements in the container
template <typename Tree>
auto find_ith_smallest(Tree& tr, size_t i){
    return tr.find_by_order(i);
}
// get iterator to ith smallest element (one-based):
// returns tr.end() if there are less than i elements in the container
template <typename Tree>
auto find_ith_largest(Tree& tr, size_t i){
    return tr.find_by_order(tr.size()-i);
}

// bool __builtin_add_overflow (type1 a, type2 b, type3 *res)
// bool __builtin_sub_overflow (type1 a, type2 b, type3 *res)
// bool __builtin_mul_overflow (type1 a, type2 b, type3 *res)
// returns true if overflow, result stored in res after casting

// int __builtin_popcount (unsigned int x) // Returns the number of 1-bits in x.
// int __builtin_ctz (unsigned int x) // Returns the number of trailing 0-bits in x, starting at the least significant bit position. If x is 0, the result is undefined.
// int __builtin_clz (unsigned int x) // Returns the number of leading 0-bits in x, starting at the most significant bit position. If x is 0, the result is undefined.

