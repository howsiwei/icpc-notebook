template <typename T, size_t N>
struct fenwick{
    T arr[N+1];
    void inc(size_t index, T val){
        ++index;
        for(;index<=N;index+=index&-index){
            arr[index]+=val;
        }
    }
    T query(size_t index){
        ++index;
        T ret=0;
        for(;index>0;index-=index&-index){
            ret+=arr[index];
        }
        return ret;
    }
};

void adjust(int a, int b, int value){
    adjust(ft1, a, value);
    adjust(ft1, b+1, -value);
    adjust(ft2, a, value * (a-1));
    adjust(ft2, b+1, -1 * value * b);
}
